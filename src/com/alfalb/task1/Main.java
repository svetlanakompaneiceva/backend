package com.alfalb.task1;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;

public class Main {

    public static final String FILE_PATH = "src/resources/test.txt";

    public static void readData(String filePath, Comparator<Long> comparator) throws IOException {
        Files.readAllLines(Paths.get(filePath))
                .stream()
                .flatMap(s-> Arrays.stream(s.split(",")))
                .map(Long::valueOf)
                .sorted(comparator)
                .forEach(System.out::println);
    }

    public static void main(String[] args) throws IOException {

        System.out.println("Output in increment order");
        readData(FILE_PATH, Comparator.naturalOrder());

        System.out.println("Output in decrement order");
        readData(FILE_PATH, Comparator.reverseOrder());

    }
}
